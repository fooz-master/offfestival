var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');

var config = {
    proxy: 'http://off-festival.local/cut',
    srcCss: [
        './cut/scss/bootstrap.scss',
        './cut/scss/main.scss'
    ],
    destCss: './cut/css',
};

gulp.task('build-css', function() {
    gulp.src(config.srcCss)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.destCss))
        .pipe(minCss())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.destCss));
});
gulp.task('browser-sync', function() {
	browserSync.init({
		files: ['./cut/*.html', './cut/*.php', './cut/css/main.min.css', './cut/css/bootstrap.min.css', './cut/js/*.js'],
		proxy: config.proxy
	});
});

// watchers
gulp.task('watch', function() {
    gulp.watch('cut/scss/**/*.scss', ['build-css']);
});


gulp.task('default', ['build-css', 'watch', 'browser-sync']);
