<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<section id="mainSlider" class="carousel slide section-slider slider-section-on-slider" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="slider-bg" style="background-image:url('img/page-dojazd-banner.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-title">Dojazd</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="py-md section-on-slider">
						<div class="label">
							DOJAZD
						</div>
						<div class="page-content content-text">
							<p>
								Na OFF Festival zapraszamy do Doliny Trzech Stawów. Zielona przestrzeń parku współtworzy atmosferę naszego festiwalu i uprzyjemnia wspólne przeżywanie muzyki. Z centrum Katowic możesz tu dogodnie dotrzeć różnymi środkami transportu.
							</p>
							<h1 class="title title-my">Jesteśmy</h1>
							<p>
								300 km od Warszawy<br>
								200 km od Wrocławia<br>
								80 km od Krakowa<br>
								520 km od Berlina<br>
								400 km od Pragi<br>
								380 km od Wiednia<br>
							</p>
							<div class="panel-group text-uppercase" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-01">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-01" aria-expanded="false" aria-controls="collapse-01">
											Samochód
										</a>
									</div>
									<div id="collapse-01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-01">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-02">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
											Pociąg
										</a>
									</div>
									<div id="collapse-02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-02">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-03">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
											Samolot
										</a>
									</div>
									<div id="collapse-03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-03">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-04">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-04" aria-expanded="false" aria-controls="collapse-04">
											Komunikacja miejska
										</a>
									</div>
									<div id="collapse-04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-04">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-05">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-05" aria-expanded="false" aria-controls="collapse-05">
											Taksówki
										</a>
									</div>
									<div id="collapse-05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-05">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-07">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-07" aria-expanded="false" aria-controls="collapse-07">
											Rowery
										</a>
									</div>
									<div id="collapse-07" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-07">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="btn-back">
								<a href="#" class="btn">Powrót do głównej</a>
							</div>
						</div>
					</section>
				</main>

				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
