<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>OFF Festival 2018</title>
	<meta name="description" content="Kto zagra, jak dojechać, program, bilety, partnerzy, materiały dla mediów. Artists, Getting there, line-up, tickets, partners, press materials." />

<!-- tu przekazywac dane z WP lub generowac w WP -->
	<meta property="og:locale" content="pl_PL">
	<meta property="og:site_name" content="">
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">

	<link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
	<link rel="manifest" href="img/favicons/site.webmanifest">
	<link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#000">
	<meta name="msapplication-TileColor" content="#000">
	<meta name="theme-color" content="#000">

	<!-- css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.min.css">

	<!-- libs -->
	<script type="text/javascript" src="lib/jquery-3.2.1.min.js"></script>

	<link rel="stylesheet" href="lib/slick.min.css">
	<script src="lib/slick.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
