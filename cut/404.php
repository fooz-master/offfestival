<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<!-- 404 only-->
<script src="404/js/inc/shim/Base64.js" type="text/javascript"></script>
<script src="404/js/inc/shim/Base64binary.js" type="text/javascript"></script>
<script src="404/js/inc/shim/WebAudioAPI.js" type="text/javascript"></script>
<script src="404/js/midi/audioDetect.js" type="text/javascript"></script>
<script src="404/js/midi/gm.js" type="text/javascript"></script>
<script src="404/js/midi/loader.js" type="text/javascript"></script>
<script src="404/js/midi/plugin.audiotag.js" type="text/javascript"></script>
<script src="404/js/midi/plugin.webaudio.js" type="text/javascript"></script>
<script src="404/js/midi/plugin.webmidi.js" type="text/javascript"></script>
<script src="404/js/util/dom_request_xhr.js" type="text/javascript"></script>
<script>document.documentElement.className = 'js';</script>
<!-- 404 only -->

<body class="page-404 subpage loading">
	<section class="intro">
		<?php include'_header.php'; ?>
		<div class="slider-spacer"></div>
		<div class="layout-grid">
			<div class="container">

				<div class="slider-spacer"></div>

				<div class="column-layout-content">
					<main id="content">
						<section class="section-page-text">
							<a href="/" class="logo">
								<img src="img/logo.svg" alt="Off Festival">
							</a>
							<div class="label">
								Błąd 404
							</div>
							<div class="content-text">
								<h1>
									Zgubiłeś się, ale to nic!
								</h1>
								<p>
									Na OFF Festivalu wszędzie czeka na Ciebie muzyka.
								</p>
								<div class="interaction interaction--light"></div>
								<div class="btn-back">
									<a href="#" class="btn">Powrót do głównej</a>
								</div>
							</div>
						</section>
					</main>
					<footer>		
						<div class="row info">
							<div class="col-xs-12 col-sm-4">
								Copyright: OFF Festival 2006-2018
							</div>
							<div class="col-xs-12 col-sm-4 col-sm-push-4 footer-right">
								Organizator festiwalu: <img src="img/footer-independent.png" alt="Fundacja Independent">
							</div>
							<div class="col-xs-12 col-sm-4 col-sm-pull-4 footer-center">
								<a href="http://creogram.com/" target="_blank" rel="nofollow">
									<img src="img/footer-creogram.png" alt="Creogram">Zaprojektowano w Creogram
								</a>
							</div>
						</div>
					</footer>
				</div>
			</div>
		</div>

	</section>
		<section class="content content--instrument content--piano content--cursor-light">
			<div class="piano instrument">
				<svg class="piano__inner" xmlns:note="http://tympanus.net/codrops" viewBox="0 0 2080 200" preserveAspectRatio="none">
					<g class="piano__keys piano__keys--natural">
						<rect note:id="0" x="0" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="2" x="40" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="3" x="80" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="5" x="120" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="7" x="160" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="8" x="200" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="10" x="240" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="12" x="280" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="14" x="320" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="15" x="360" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="17" x="400" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="19" x="440" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="20" x="480" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="22" x="520" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="24" x="560" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="26" x="600" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="27" x="640" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="29" x="680" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="31" x="720" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="32" x="760" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="34" x="800" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="36" x="840" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="38" x="880" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="39" x="920" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="41" x="960" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="43" x="1000" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="44" x="1040" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="46" x="1080" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="48" x="1120" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="50" x="1160" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="51" x="1200" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="53" x="1240" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="55" x="1280" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="56" x="1320" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="58" x="1360" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="60" x="1400" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="62" x="1440" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="63" x="1480" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="65" x="1520" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="67" x="1560" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="68" x="1600" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="70" x="1640" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="72" x="1680" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="74" x="1720" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="75" x="1760" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="77" x="1800" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="79" x="1840" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="80" x="1880" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="82" x="1920" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="84" x="1960" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="86" x="2000" class="piano__key piano__key--natural" width="40" height="200"/>
						<rect note:id="87" x="2040" class="piano__key piano__key--natural" width="40" height="200"/>
					</g>
					<g class="piano__keys piano__keys--flat">
						<rect note:id="1" x="30" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="4" x="110" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="6" x="150" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="9" x="230" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="11" x="270" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="13" x="310" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="16" x="390" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="18" x="430" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="21" x="510" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="23" x="550" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="25" x="590" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="28" x="670" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="30" x="710" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="33" x="790" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="35" x="830" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="37" x="870" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="40" x="950" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="42" x="990" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="45" x="1070" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="47" x="1110" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="49" x="1150" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="52" x="1230" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="54" x="1270" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="57" x="1350" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="59" x="1390" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="61" x="1430" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="64" x="1510" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="66" x="1550" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="69" x="1630" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="71" x="1670" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="73" x="1710" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="76" x="1790" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="78" x="1830" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="81" x="1910" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="83" x="1950" class="piano__key piano__key--flat" width="20" height="130"/>
						<rect note:id="85" x="1990" class="piano__key piano__key--flat" width="20" height="130"/>
					</g>
				</svg>
			</div><!-- piano -->
		</section>
	<script src="404/js/mousetrap.min.js"></script>
	<script src="404/js/enquire.min.js"></script>
	<!-- <script src="404/js/anime.min.js"></script> -->
	<script src="404/js/helper.js"></script>
	<script src="404/js/piano.js"></script>
	<script src="404/js/main.js"></script>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
