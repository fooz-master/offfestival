<article class="element-loop col-xs-12 col-sm-6 grid-item">
	<div class="news-item">
		<a href="#" class="news-thumb">
			<img src="img/news-thumb.jpg" alt="">
		</a>
		<div class="box-inner">
			<a href="" class="news-link" title="">
				<h3>Nowa lista potwierdzonych artystów tegorocznej edycji</h3>
			</a>
			<div class="news-date">
				04.04.2018
			</div>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit.
			</p>
			<div class="news-socials">
				<a href="#" target="_blank">
					<?php include'_svg-social-fb.php'; ?>
				</a>
				<a href="#" target="_blank">
					<?php include'_svg-social-tw.php'; ?>
				</a>
			</div>
		</div>
		<div class="read-more-mask">
			<a href="#" title="" class="read-more">
				CZYTAJ WIĘCEJ
			</a>
		</div>
	</div>
</article>
