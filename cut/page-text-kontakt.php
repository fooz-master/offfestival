<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-text">
						<div class="label">
							Kontakt
						</div>
						<div class="content-text">
							<div class="lead">
								<h1>
									Pytania? Jesteśmy tutaj dla Ciebie.
								</h1>
								<p>
									W pierwszej kolejności zapoznaj się z naszą sekcją FAQ. Jeśli nadal masz wątpliwości, skontaktuj się z nami za pośrednictwem maila.
								</p>
							</div>

<!--
INFO: podstrony page-text-***.php i page-banner-***.php w .panel-group mogę posiadac w sobie zakładki do rozwiniecia,  buttony do pobrania, albo statyczny tekst w nagłówkiem. Te elementy zbudowane sa na boostrapowych elementach .panel Dzięki temu w jednym .panel-group można je mieszac i strona styluje sie odpowienio
INFO: niektóre podstrony z accordionem mają dodaną klase uppercase do .panel-group. Styluje tak nagłówki pisane wielkimi, raz malymi literami
-->

							<div class="panel-group text-uppercase" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-01">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-01" aria-expanded="false" aria-controls="collapse-01">
											Muzyka
										</a>
									</div>
									<div id="collapse-01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-01">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-02">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
											Gastronomia
										</a>
									</div>
									<div id="collapse-02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-02">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-03">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
											Off Market
										</a>
									</div>
									<div id="collapse-03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-03">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-04">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-04" aria-expanded="false" aria-controls="collapse-04">
											Niepełnosprawni
										</a>
									</div>
									<div id="collapse-04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-04">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-05">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-05" aria-expanded="false" aria-controls="collapse-05">
											Sponsoring
										</a>
									</div>
									<div id="collapse-05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-05">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-06">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-06" aria-expanded="false" aria-controls="collapse-06">
											Wolontariat
										</a>
									</div>
									<div id="collapse-06" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-06">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-07">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-07" aria-expanded="false" aria-controls="collapse-07">
											Informacje ogólne
										</a>
									</div>
									<div id="collapse-07" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-07">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
											<p class="text-right">
												<a href="mailto:" class="btn btn-lg btn-primary">
													adres@email.pl
												</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
