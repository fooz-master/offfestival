<footer>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="foot-label">
				GOSPODARZE FESTIWALU
			</div>
			<div class="fbox">
				<div class="slick slick-footer-small">
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
					<div>
						<img src="img/footer-partnerzy.png" alt="">
					</div>
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
					<div>
						<img src="img/footer-partnerzy.png" alt="">
					</div>
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="foot-label">
				PARTNERZY
			</div>
			<div class="fbox">
				<div class="slick slick-footer-small">
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
					<div>
						<img src="img/footer-partnerzy.png" alt="">
					</div>
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
					<div>
						<img src="img/footer-partnerzy.png" alt="">
					</div>
					<div>
						<img src="img/footer-patroni.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="foot-label">
				PATRONI MEDIALNI
			</div>
			<div class="fbox">
				<div class="slick slick-footer-big">
					<div>
						<img src="img/footer-media-trojka.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-cojestgrane24.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-gazetapl.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-ams.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-polityka.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-vice.png" alt="">
					</div>
					<div>
						<img src="img/footer-media-empik.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row info">
		<div class="col-xs-12 col-sm-4">
			Copyright: OFF Festival 2006-2018
		</div>
		<div class="col-xs-12 col-sm-4 col-sm-push-4 footer-right">
			Organizator festiwalu: <img src="img/footer-independent.png" alt="Fundacja Independent">
		</div>
		<div class="col-xs-12 col-sm-4 col-sm-pull-4 footer-center">
			<a href="http://creogram.com/" target="_blank" rel="nofollow">
				<img src="img/footer-creogram.png" alt="Creogram">Zaprojektowano w Creogram
			</a>
		</div>
	</div>
</footer>
