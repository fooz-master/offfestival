<div class="collapse navbar-collapse column-layout-menu" id="mainmenu">
	<div class="scroll-mobile">
		<a href="#" class="logo-mobile">
			<img src="img/logo-black.svg" alt="OFF Festival">
		</a>
		<button type="button" class="navbar-toggle navbar-close" data-toggle="collapse" data-target="#mainmenu">
			<span class="sr-only">Toggle navigation</span>
			<img src="img/menu-close.svg" alt="">
		</button>
		<nav class="main-menu" role="navigation">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Program</a>
					<ul class="dropdown-menu">
						<li><a href="#">ARTYŚCI</a></li>
						<li><a href="#">LINE UP</a></li>
						<li><a href="#">WYDARZENIA DODATKOWE</a></li>
						<li><a href="#">STREFA GASTRO</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O Festivalu</a>
					<ul class="dropdown-menu">
						<li><a href="#">HISTORIA</a></li>
						<li><a href="#">AMBASADORZY</a></li>
						<li><a href="#">SPONSORZY</a></li>
						<li><a href="#">MEDIA</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Aktualności</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Info</a>
					<ul class="dropdown-menu dropdown-menu-columns">
						<li><a href="#">DOJAZD</a></li>
						<li><a href="#">WOLONTARIAT</a></li>
						<li><a href="#">NOCLEGI</a></li>
						<li><a href="#">NIEPEŁNOSPRAWNI</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">KONTAKT</a></li>
						<li><a href="#">REGULAMINY</a></li>
					</ul>
				</li>
			</ul>
			<div class="menu-lang">
				Language: <a href="#">English</a>
			</div>
		</nav>
		<div class="menu-socials">
			<a href="#" target="_blank">
				<?php include'_svg-social-fb.php'; ?>
			</a>
			<a href="#" target="_blank">
				<?php include'_svg-social-yt.php'; ?>
			</a>
			<a href="#" target="_blank">
				<?php include'_svg-social-insta.php'; ?>
			</a>
			<a href="#" target="_blank">
				<?php include'_svg-social-tw.php'; ?>
			</a>
			<a href="#" target="_blank">
				<?php include'_svg-social-spotify.php'; ?>
			</a>
		</div>
	</div>
	<div class="menu-download">
		<a href="#" class="col-xs-6 text-center link-download">
			<img src="img/menu-download-android.png" class="img-responsive" alt="OFF Festival - Android Play">
		</a>
		<a href="#" class="col-xs-6 text-center link-download">
			<img src="img/menu-download-ios.png" class="img-responsive" alt="OFF Festival - iStore">
		</a>
	</div>
</div>
