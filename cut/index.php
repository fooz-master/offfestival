<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="home page">
	<?php include'_header.php'; ?>

	<section id="mainSlider" class="carousel slide section-slider" data-ride="carousel">
		<div class="carousel-inner">
			<!-- <div class="item active">
				<div class="slider-bg" style="background-image:url('img/slider-bg.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-small">03 – 05.08<br>2018</div>
										<div class="slider-title">Dolina Trzech Stawów</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="slider-bg" style="background-image:url('img/slider-bg2.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-small">03 – 05.08<br>2018</div>
										<div class="slider-title">Dolina Trzech Stawów</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div> -->

			<!-- YT video opction for slider -->
			<div class="item active">
				<div class="slider-bg">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-small">03 – 05.08<br>2018</div>
										<div class="slider-title">Dolina Trzech Stawów</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
					<div class="video-background">
						<div class="video-foreground">
							<iframe src="https://www.youtube.com/embed/W0LHTWG-UmQ?autoplay=1&loop=1&playlist=W0LHTWG-UmQ" allow="autoplay; encrypted-media" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="py-md section-home-artists">
						<div class="label">
							Artyści
						</div>
						<div class="box-inner">
							<a href="#">Swans</a>
							<a href="#">PJ Harvey</a>
							<a href="#">Feist</a>
							<a href="#">Talib Kweli Live</a>
							<a href="#">Shellac</a>
							<a href="#">Pro8l3m</a>
							<a href="#">Conor Oberst with the band</a>
							<a href="#"><strong>Kwadrofonik &amp; Artur Rojek</strong></a>
						</div>
						<a href="#" class="link-more-primary">Zobacz wszystkich artystów</a>
					</section>

					<div class="background-gray-lighter full_width_element">
						<section class="py-lg section-home-news">
							<div class="container">
								<div class="column-layout-spacer"></div>
								<div class="column-layout-content">
									<div class="label">
										Newsy
									</div>
									<div class="row row-gutter-md">
										<?php include'_news-item1.php'; ?>
										<?php include'_news-item2.php'; ?>
									</div>
									<div class="info-link visible-xs visible-sm">
										<a href="#" class="link-more-primary">Zobacz wszystkie aktualności</a>
									</div>
								</div>
							</div>
						</section>

						<section class="py-md pt-none section-tickets">
							<div class="container">
								<div class="column-layout-spacer"></div>
								<div class="column-layout-content">
									<div class="row row-tickets">
										<div class="grid-ticket-type">
											<div class="box-ticket hidden-xs">
												KARNET 3 DNIOWY
											</div>
										</div>
										<div class="grid-ticket-type visible-xs">
											<button class="box-ticket collapsed" type="button" data-toggle="collapse" data-target="#karnet-3dni">
												KARNET 3 DNIOWY
											</button>
										</div>
										<div class="collapse" id="karnet-3dni">
											<div class="grid-ticket-prize">
												<div class="box-ticket">
													<span class="visible-xs-inline">Cena:&nbsp;</span> 260 PLN
												</div>
											</div>
											<div class="grid-ticket-buy">
												<a href="#" class="box-ticket">
													<?php include'_svg-ticket.php'; ?> KUP BILET
												</a>
											</div>
										</div>
									</div>
									<!-- <div class="row row-tickets">
										<div class="grid-ticket-type">
											<div class="box-ticket hidden-xs">
												KARNET JEDNODNIOWY
											</div>
										</div>
										<div class="grid-ticket-type visible-xs">
											<button class="box-ticket collapsed" type="button" data-toggle="collapse" data-target="#karnet-1dzien">
												KARNET JEDNODNIOWY
											</button>
										</div>
										<div class="collapse" id="karnet-1dzien">
											<div class="grid-ticket-prize">
												<div class="box-ticket">
													<span class="visible-xs-inline">Cena:&nbsp;</span> 130 PLN
												</div>
											</div>
											<div class="grid-ticket-buy">
												<a href="#" class="box-ticket">
													<?php include'_svg-ticket.php'; ?> KUP BILET
												</a>
											</div>
										</div>
									</div> -->
									<div class="row row-tickets">
										<div class="grid-ticket-type">
											<div class="box-ticket hidden-xs">
												POLE NAMIOTOWE
											</div>
										</div>
										<div class="grid-ticket-type visible-xs">
											<button class="box-ticket collapsed" type="button" data-toggle="collapse" data-target="#karnet-pole">
												POLE NAMIOTOWE
											</button>
										</div>
										<div class="collapse" id="karnet-pole">
											<div class="grid-ticket-prize">
												<div class="box-ticket">
													<span class="visible-xs-inline">Cena:&nbsp;</span> 70 PLN
												</div>
											</div>
											<div class="grid-ticket-buy">
												<a href="#" class="box-ticket">
													<?php include'_svg-ticket.php'; ?> KUP BILET
												</a>
											</div>
										</div>
									</div>
									<div class="info">
										Podane ceny obowiązują od 18.12.2017 - 18.02.2018
									</div>
									<div class="buy-tickets-portale">
										<p class="title">
											Bilety w sprzedaży:
										</p>
										<a href="http://offsklep.pl" target="_blank" rel="nofollow">offsklep.pl</a>
										<a href="http://www.ebilet.pl" target="_blank" rel="nofollow">ebilet.pl</a>
										<a href="http://www.ticketmaster.pl" target="_blank" rel="nofollow">ticketmaster.pl</a>
									</div>
								</div>
							</div>
						</section>
					</div>
				</main>

				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
