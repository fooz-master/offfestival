<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-people-listing">
						<div class="label">
							Artyści
						</div>
						<div class="sorting clearfix">
							<div class="sorting-category">
								Kategoria:
								<div class="dropdown dropdown-button">
									<button id="dropKategorie" class="btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Wszystkie
									 </button>
									 <span class="caret"></span>
									<div class="dropdown-menu" aria-labelledby="dropKategorie">
										<a href="#">Kategoria 1</a>
										<a href="#">Kategoria 2</a>
										<a href="#">Kategoria 3</a>
									</div>
								</div>
							</div>
							<div class="sorting-icons">
								<a href="#">
									<img src="img/sorting-grid.svg" alt="">
								</a>
								<a href="#">
									<img src="img/sorting-list.svg" alt="">
								</a>
							</div>
						</div>
						<div class="people-view-grid">
							<div class="row row-gutter-xl row-eq-height">
								<?php include'_people-item.php'; ?>
								<?php include'_people-item.php'; ?>
							</div>
							<div class="row row-gutter-xl row-eq-height">
								<?php include'_people-item.php'; ?>
								<?php include'_people-item.php'; ?>
							</div>
							<div class="row row-gutter-xl row-eq-height">
								<?php include'_people-item.php'; ?>
								<?php include'_people-item.php'; ?>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
