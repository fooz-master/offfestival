<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<section id="mainSlider" class="carousel slide section-slider slider-section-on-slider" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="slider-bg" style="background-image:url('img/page-wydarzenia-banner.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-title">Kawiarnia Literacka</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-single section-on-slider">
						<div class="label">
							KAWIARNIA LITERACKA
						</div>
						<div class="single-featured-image" style="background-image:url('img/page-kawiarnia.jpg')">
						</div>
						<div class="single-layout clearfix">
							<div class="single-socials">
								<a href="#" class="icon icon-download" target="_blank">
									<img src="img/slider-arrow.svg" alt="">
									POBIERZ<br>PROGRAM
								</a>
							</div>
							<div class="page-content single-content content-text">
								<h2>
									Wszyscy – to słowo, które łączy spotkania w tegorocznej kawiarni Literackiej. Jak wyjaśnia Sylwia Chutnik, kuratorka, chodzi o to, by „na chwilę zapomnieć o podziałach i skupić się na tym, co nas łączy”. Partnerem Kawiarni Literackiej na OFFie jest Empik.
								</h2>
								<hr>
								<p>
									Tegoroczna Kawiarnia Literacka będzie nawiązywała do atmosfery czytania w domu. Gdzieś na łóżku lub w wygodnym fotelu. Spokojnie i wbrew natłokowi pracy i codziennej szamotaninie. Motywem przewodnim naszych literackich rozmów jest wspólnota. Na chwilę zawiesimy spory i podziały zastanawiając się, czy jest coś, co nas łączy i sprawia, że możemy powiedzieć: „wszyscy”?
								</p>
								<p>
									Zapraszamy na debaty i słuchowiska, czyli czytanie książek przez ich autorów i autorki wraz z akompaniamentem muzyków. W trakcie festiwalu zobaczyć będzie można wystawę okładek książek zaproszonych gości, w ramach akcji „Oceń książkę po okładce”. W namiocie Kawiarni znajdować się będzie również księgarnia, gdzie będzie można oddać się najważniejszej czynności na świecie: kompulsywnym zakupom książek
								</p>
								<div class="single-socials">
									<?php include'_page-single-socials.php'; ?>
								</div>
								<div class="panel-group text-uppercase" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="heading-01">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-01" aria-expanded="false" aria-controls="collapse-01">
												Piątek
											</a>
										</div>
										<div id="collapse-01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-01">
											<div class="panel-body">
												<p>
													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
												</p>
												<div class="content-photo">
													<img src="img/content-photo.jpg" alt="">
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="heading-02">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
												Sobota
											</a>
										</div>
										<div id="collapse-02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-02">
											<div class="panel-body">
												<p>
													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
												</p>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="heading-03">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
												Niedziela
											</a>
										</div>
										<div id="collapse-03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-03">
											<div class="panel-body">
												<p>
													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
												</p>
											</div>
										</div>
									</div>
								</div>

								<div class="btn-back">
									<a href="#" class="btn">Powrót do wydarzeń</a>
								</div>
							</div>
						</div>
					</section>
				</main>

				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
