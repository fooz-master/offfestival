<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-single">
						<div class="label">
							Newsy
						</div>
						<div class="single-featured-image" style="background-image:url('img/single-featured-image-news.jpg')">
						</div>
						<div class="single-layout clearfix">
							<div class="single-socials">
								<?php include'_page-single-socials-short.php'; ?>
							</div>
							<div class="page-content single-content single-content-news content-text">
								<h1 class="title">
									Cześć! Polecamy zamienienie się w słuch, bo Artur ma dla Was ważne wieści.
								</h1>
								<div class="date">
									12.02.2018
								</div>
								<hr>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<blockquote>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
								</blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<div class="single-socials">
									<?php include'_page-single-socials.php'; ?>
								</div>
								<div class="tags">
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
									<a href="#">lorem</a>, <a href="#">lorem</a>,
								</div>
								<div class="btn-back">
									<a href="#" class="btn">Powrót do newsów</a>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
