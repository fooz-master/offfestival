<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage history">
	<?php include'_header.php'; ?>

	<section id="mainSlider" class="carousel slide section-slider slider-section-on-slider" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="slider-bg" style="background-image:url('img/page-historia-banner.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-title">Dojazd</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="py-md section-on-slider">
						<div class="label">
							HISTORIA
						</div>
						<div class="page-content content-text">
							<article>
								<h2 class="title">
									Artur Rojek
								</h2>
								<strong>
									Artur Rojek , pomysłodawca , twórca i dyrektor artystyczny OFF Festivalu
								</strong>
								<div class="collapse desc-collapse" id="description-01">
									<div class="inner">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<button class="btn btn-desc-collapse collapsed" type="button" data-toggle="collapse" data-target="#description-01" aria-expanded="false"></button>
							</article>

							<article>
								<div class="slick slick-content-photo content-photo">
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
								</div>
							</article>

							<article>
								<h2 class="title">
									Nagłówek 2
								</h2>
								<strong>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit
								</strong>
								<div class="collapse desc-collapse" id="description-02">
									<div class="inner">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<button class="btn btn-desc-collapse collapsed" type="button" data-toggle="collapse" data-target="#description-02" aria-expanded="false"></button>
							</article>

							<article>
								<div class="slick slick-content-photo content-photo">
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
								</div>
							</article>

							<article>
								<h2 class="title">
									Nagłówek 3
								</h2>
								<strong>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit
								</strong>
								<div class="collapse desc-collapse" id="description-03">
									<div class="inner">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<button class="btn btn-desc-collapse collapsed" type="button" data-toggle="collapse" data-target="#description-03" aria-expanded="false"></button>
							</article>

							<article>
								<div class="slick slick-content-photo content-photo">
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
								</div>
							</article>

							<article>
								<h2 class="title">
									Nagłówek 4
								</h2>
								<strong>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit
								</strong>
								<div class="collapse desc-collapse" id="description-04">
									<div class="inner">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate aaa velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<button class="btn btn-desc-collapse collapsed" type="button" data-toggle="collapse" data-target="#description-04" aria-expanded="false"></button>
							</article>

							<div class="btn-back">
								<a href="#" class="btn">Powrót do głównej</a>
							</div>
						</div>
					</section>
				</main>

				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>

</body>

<?php include '_footer-scripts.php'; ?>

</html>
