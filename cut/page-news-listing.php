<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-news-listing">
						<div class="label">
							Newsy
						</div>
						<div class="grid row row-gutter-md fix-masonry-listing">
							<?php include'_news-item1.php'; ?>
							<?php include'_news-item2.php'; ?>
							<?php include'_news-item3.php'; ?>
							<?php include'_news-item1.php'; ?>
							<?php include'_news-item2.php'; ?>
							<?php include'_news-item3.php'; ?>
						</div>
					</section>
				</main>

				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

<!-- masonry only on news -->
<script src="lib/masonry.pkgd.min.js"></script>
<script type="text/javascript">
	$('.grid').masonry({
		itemSelector: '.grid-item'
	});
</script>

</html>
