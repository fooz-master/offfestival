<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 23" width="11" height="23" class="iconsvg iconsvg-fb">
	<g>
		<path d="M2.38,4.45V7.62H0v3.87H2.38V23H7.26V11.49h3.28s.31-1.85.45-3.88H7.28V5A1.14,1.14,0,0,1,8.34,4H11V0H7.38C2.26,0,2.38,3.88,2.38,4.45Z"/>
	</g>
</svg>
