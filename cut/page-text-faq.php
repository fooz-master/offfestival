<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-text">
						<div class="label">
							FAQ
						</div>
						<div class="content-text">
							<div class="lead">
								<h1>
									Festiwal
								</h1>
								<!-- <p>
									W pierwszej kolejności zapoznaj się z naszą sekcją FAQ. Jeśli nadal masz wątpliwości, skontaktuj się z nami za pośrednictwem maila.
								</p> -->
							</div>
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-01">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-01" aria-expanded="false" aria-controls="collapse-01">
											Czy osoby niepełnoletnie mają wstęp na festiwal?
										</a>
									</div>
									<div id="collapse-01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-01">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-02">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
											O której godzinie otwierają się bramy festiwalu?
										</a>
									</div>
									<div id="collapse-02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-02">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-03">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
											Czy pod scenami przewidziane są wydzielone strefy dla osób z niepełnosprawnościami?
										</a>
									</div>
									<div id="collapse-03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-03">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-04">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-04" aria-expanded="false" aria-controls="collapse-04">
											Czy osoba z niepełnosprawnościami może wejść na teren festiwalu wraz z opiekunem?
										</a>
									</div>
									<div id="collapse-04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-04">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-05">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-05" aria-expanded="false" aria-controls="collapse-05">
											Czy na teren festiwalu można wejść z psem?
										</a>
									</div>
									<div id="collapse-05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-05">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-06">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-06" aria-expanded="false" aria-controls="collapse-06">
											Czy na terenie festiwalu będzie można naładować akumulator
do wózka inwalidzkiego?
										</a>
									</div>
									<div id="collapse-06" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-06">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-07">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-07" aria-expanded="false" aria-controls="collapse-07">
											Czy osoby chore np. na cukrzycę mogą zabrać ze sobą lekarstwa
i konieczne przybory medyczne lub wnieść własne jedzenie?
										</a>
									</div>
									<div id="collapse-07" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-07">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-08">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-08" aria-expanded="false" aria-controls="collapse-08">
											Czy na terenie festiwalu znajdują się służby medyczne?
										</a>
									</div>
									<div id="collapse-08" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-08">
										<div class="panel-body">
											<p>
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
