<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<section id="mainSlider" class="carousel slide section-slider slider-section-on-slider" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				<div class="slider-bg" style="background-image:url('img/page-wydarzenia-lista-banner.jpg');">
					<div class="container">
						<div class="column-layout-spacer"></div>
						<div class="column-layout-content">
							<div class="slider-content">
								<div class="slider-text-mask">
									<div class="slider-text">
										<div class="slider-small">OFFowe</div>
										<div class="slider-title">Dodatkowe wydarzenia</div>
									</div>
								</div>
								<div class="slider-ticket">
									<a href="#" class="buy-ticket">
										<?php include'_svg-ticket.php'; ?>
										<span>Kup bilet</span>
									</a>
								</div>
							</div>
							<a href="#content" class="slider-arrow">
								<img src="img/slider-arrow.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="py-md section-on-slider">
						<div class="label">
							WYDARZENIA DODATKOWE
						</div>
						<div class="page-content content-text">
							<article>
								<h2 class="title"><a href="#">Kawiarnia literacka</a></h2>
								<p>
									Z kolei w Kawiarni Literackiej nie zabraknie tematów poświęconych literaturze współczesnej. Rozmowy toczą się o pisaniu, reportażach. Tegoroczna Kawiarnia Literacka nawiązuje do atmosfery czytania w domu. Gdzieś na łóżku lub w wygodnym fotelu. Spokojnie, wbrew natłokowi pracy i codziennej szamotaninie przy kawie organizatorzy zapraszają na czytanie i słuchowiska.
								</p>
							</article>
							<article>
								<h2 class="title"><a href="#">Kawiarnia literacka</a></h2>
								<p>
									Z kolei w Kawiarni Literackiej nie zabraknie tematów poświęconych literaturze współczesnej. Rozmowy toczą się o pisaniu, reportażach. Tegoroczna Kawiarnia Literacka nawiązuje do atmosfery czytania w domu. Gdzieś na łóżku lub w wygodnym fotelu. Spokojnie, wbrew natłokowi pracy i codziennej szamotaninie przy kawie organizatorzy zapraszają na czytanie i słuchowiska.
								</p>
							</article>
							<article>
								<h2 class="title"><a href="#">Kawiarnia literacka</a></h2>
								<p>
									Z kolei w Kawiarni Literackiej nie zabraknie tematów poświęconych literaturze współczesnej. Rozmowy toczą się o pisaniu, reportażach. Tegoroczna Kawiarnia Literacka nawiązuje do atmosfery czytania w domu. Gdzieś na łóżku lub w wygodnym fotelu. Spokojnie, wbrew natłokowi pracy i codziennej szamotaninie przy kawie organizatorzy zapraszają na czytanie i słuchowiska.
								</p>
							</article>
							<article>
								<div class="slick slick-content-photo content-photo">
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
									<div>
										<img src="img/content-photo.jpg" alt="">
									</div>
								</div>
							</article>
							<article>
								<h2 class="title"><a href="#">Kawiarnia literacka</a></h2>
								<p>
									Z kolei w Kawiarni Literackiej nie zabraknie tematów poświęconych literaturze współczesnej. Rozmowy toczą się o pisaniu, reportażach. Tegoroczna Kawiarnia Literacka nawiązuje do atmosfery czytania w domu. Gdzieś na łóżku lub w wygodnym fotelu. Spokojnie, wbrew natłokowi pracy i codziennej szamotaninie przy kawie organizatorzy zapraszają na czytanie i słuchowiska.
								</p>
							</article>
							<div class="btn-back">
								<a href="#" class="btn">Powrót do głównej</a>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
