<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-text">
						<div class="label">
							Media
						</div>
						<div class="content-text">
							<!--
							<div class="lead">
								<h1>
									Festiwal
								</h1>
								<p>
									W pierwszej kolejności zapoznaj się z naszą sekcją FAQ. Jeśli nadal masz wątpliwości, skontaktuj się z nami za pośrednictwem maila.
								</p>
							</div>
							 -->

<!-- INFO: dodana klasa .panel-group-list do .panel-group -->

							<div class="panel-group panel-group-list text-uppercase" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-01">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-01" aria-expanded="false" aria-controls="collapse-01">
											Formularz akredytacyjny
										</a>
									</div>
									<div id="collapse-01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-01">
										<div class="panel-body">
											<ul>
												<li>
													<a href="#">Przykładowy plik do pobrania</a>
												</li>
												<li>
													<a href="#">Drugi plik do pobrania</a>
												</li>
												<li>
													<a href="#">Trzeci plik do pobrania</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-02">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-02" aria-expanded="false" aria-controls="collapse-02">
											Bio festiwalu
										</a>
									</div>
									<div id="collapse-02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-02">
										<div class="panel-body">
											<ul>
												<li>
													<a href="#">Przykładowy plik do pobrania</a>
												</li>
												<li>
													<a href="#">Drugi plik do pobrania</a>
												</li>
												<li>
													<a href="#">Trzeci plik do pobrania</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-03">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-03" aria-expanded="false" aria-controls="collapse-03">
											OFF 2018
										</a>
									</div>
									<div id="collapse-03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-03">
										<div class="panel-body">
											<ul>
												<li>
													<a href="#">Przykładowy plik do pobrania</a>
												</li>
												<li>
													<a href="#">Drugi plik do pobrania</a>
												</li>
												<li>
													<a href="#">Trzeci plik do pobrania</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-04">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-04" aria-expanded="false" aria-controls="collapse-04">
											OFF 2017
										</a>
									</div>
									<div id="collapse-04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-04">
										<div class="panel-body">
											<ul>
												<li>
													<a href="#">Przykładowy plik do pobrania</a>
												</li>
												<li>
													<a href="#">Drugi plik do pobrania</a>
												</li>
												<li>
													<a href="#">Trzeci plik do pobrania</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-05">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-05" aria-expanded="false" aria-controls="collapse-05">
											OFF 2016
										</a>
									</div>
									<div id="collapse-05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-05">
										<div class="panel-body">
											<ul>
												<li>
													<a href="#">Przykładowy plik do pobrania</a>
												</li>
												<li>
													<a href="#">Drugi plik do pobrania</a>
												</li>
												<li>
													<a href="#">Trzeci plik do pobrania</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
