<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-people-listing">
						<div class="label">
							Artyści
						</div>
						<div class="sorting clearfix">
							<div class="sorting-category">
								Kategoria:
								<div class="dropdown dropdown-button">
									<button id="dropKategorie" class="btn-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Wszystkie
									 </button>
									 <span class="caret"></span>
									<div class="dropdown-menu" aria-labelledby="dropKategorie">
										<a href="#">Kategoria 1</a>
										<a href="#">Kategoria 2</a>
										<a href="#">Kategoria 3</a>
									</div>
								</div>
							</div>
							<div class="sorting-icons">
								<a href="#">
									<img src="img/sorting-grid.svg" alt="">
								</a>
								<a href="#">
									<img src="img/sorting-list.svg" alt="">
								</a>
							</div>
						</div>
						<div class="people-view-az">
							<div class="row row-az">
								<div class="col-xs-12 col-sm-3 col-md-2">
									<span class="big-letter">A</span>
								</div>
								<div class="col-xs-12 col-sm-9 col-md-10">
									<div class="row row-100">
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
									</div>
								</div>
							</div>
							<div class="row row-az">
								<div class="col-xs-12 col-sm-3 col-md-2">
									<span class="big-letter">B</span>
								</div>
								<div class="col-xs-12 col-sm-9 col-md-10">
									<div class="row row-100">
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
									</div>
								</div>
							</div>
							<div class="row row-az">
								<div class="col-xs-12 col-sm-3 col-md-2">
									<span class="big-letter">C</span>
								</div>
								<div class="col-xs-12 col-sm-9 col-md-10">
									<div class="row row-100">
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
										<?php include'_people-item.php'; ?>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
