// height picker with delay
var rtime;
var timeout = false;
var delta = 100;

$(window).resize(function() {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        sliderSpacer();
        hegihtSliderContent();
        widthMenuDropdown();
        fullWidthElement();
        roweqheight();
        stickyHeaderPageType();
        sectionHomeNewsHeight();
		menuInvertOnScroll();
    }
}
$(document).ready(function() {
    sliderSpacer();
    hegihtSliderContent();
    widthMenuDropdown();
    fullWidthElement();
    sectionHomeNewsHeight();
    roweqheight();
    stickyHeaderPageType();
	menuInvertOnScroll();
});
$(window).on('load', function() {
    sliderSpacer();
    hegihtSliderContent();
	sliderAnimation();
    widthMenuDropdown();
    fullWidthElement();
    sectionHomeNewsHeight();
    roweqheight();
    stickyHeaderPageType();
    stickyHeaderOnScroll();
    stickyTicketOnScroll();
});
$(window).on('scroll', function() {
    stickyHeaderOnScroll();
    stickyTicketOnScroll();
});

// menu background black after scroll
function menuInvertOnScroll() {
    //caches a jQuery object
    var heightSectionSlider = $('.section-slider').outerHeight();
	var heightSectionArtists = $('.section-home-artists').outerHeight();
	var heightSectionNews = $('.section-home-news').outerHeight();
	var heightMenu = $('.column-layout-menu').outerHeight();
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= heightSectionSlider + heightSectionArtists - heightMenu) {
            $(".column-layout-menu").addClass("menu-invert");
        }  else {
            $(".column-layout-menu").removeClass("menu-invert");
        }
    });
}

// sticky header
function stickyHeaderOnScroll() {
    if ($('body').hasClass('home')) {
        if ($(window).scrollTop() > 200) {
            $('.header-sticky').addClass('sticked');
        } else {
            $('.header-sticky').removeClass('sticked');
        }
    }
}

function stickyTicketOnScroll() {
    if ($(window).scrollTop() > 200) {
        $('.ticket-sticky').addClass('sticked');
    } else {
        $('.ticket-sticky').removeClass('sticked');
    }
}

// sticky header for subpage
function stickyHeaderPageType() {
    if ($(window).outerWidth() < 1320) {
        if ($('body').hasClass('subpage')) {
            $('.header-sticky').addClass('sticked');
            $('header').addClass('hidden');
        }
    } else {
        $('.header-sticky').removeClass('sticked');
        $('header').removeClass('hidden');
    }
}

// animacja tekstu na sliderze
function sliderAnimation() {
    if ($('.section-slider').length) {
        $('.section-slider').addClass('animated');
    }
}

// spacer dla contentu jeżeli istnieje slider
function sliderSpacer() {
    if ($('.section-slider').length) {
        if ($(window).outerWidth() >= 1320) {
            var heightHeader = $('header').outerHeight();
            var heightSlider = $('.section-slider').outerHeight();
			var heightMenu = $('#mainmenu').outerHeight();
            var heightSpacer = heightSlider - heightHeader - heightMenu;
            $('.slider-spacer').css('height', heightSpacer + 'px');
        } else {
            $('.slider-spacer').css('display', 'none');
        }
    } else {
		$('.slider-spacer').css('display', 'none');
	}
}

// ustalenie wysokości content slidera
function hegihtSliderContent() {
    if ($(window).outerWidth() >= 1320) {
        var heightMenu = $('.main-menu').outerHeight();
        $('.section-slider .slider-content').css('height', heightMenu + 'px');
    } else {
        $('.section-slider .slider-content').css('height', 'auto');
    }
}

// ustalenie szerokości dropdown menu na desktop
function widthMenuDropdown() {
    if ($(window).outerWidth() >= 1320) {
        var widthContainer = $('.container').width();
        var widthMenu = $('.main-menu').outerWidth();
        var widthMenuDropdown = widthContainer - widthMenu;
        $('.main-menu .dropdown-menu').css('width', widthMenuDropdown + 'px');
    } else {
        $('.main-menu .dropdown-menu').css('width', 'auto');
    }
}

// home news - set the heighest height
function sectionHomeNewsHeight() {
    jQuery('.home .section-home-news').each(function() {
        jQuery(this).find('.box-inner').css('min-height', 'auto');
        if ($(window).outerWidth() > 768) {
            var seth = 0;
            jQuery(this).find('.box-inner').each(function() {
                newh = jQuery(this).outerHeight();
                if (newh > seth) {
                    seth = newh;
                }
            });
            jQuery(this).find('.box-inner').css('min-height', seth);
        } else {
            jQuery(this).find('.box-inner').css('min-height', 'auto');
        }
    });
}

// set the heighest height
function roweqheight() {
    jQuery('.people-view-grid .row-eq-height').each(function() {
        jQuery(this).find('.people-link').css('min-height', 'auto');
        if ($(window).outerWidth() > 768) {
            var seth = 0;
            jQuery(this).find('.people-link').each(function() {
                newh = jQuery(this).outerHeight();
                if (newh > seth) {
                    seth = newh;
                }
            });
            jQuery(this).find('.people-link').css('min-height', seth);
        } else {
            jQuery(this).find('.people-link').css('min-height', 'auto');
        }
    });
}

// fullwidth element
function fullWidthElement() {
    var wrapper = jQuery('body');
    jQuery('.full_width_element').each(function() {
        jQuery(this).css({
            "margin-left": 0,
            "margin-right": 0,
            "padding-left": 0,
            "padding-right": 0
        });
        var left = jQuery(this).offset().left;
        var right = (jQuery(window).width() - (jQuery(this).offset().left + jQuery(this).outerWidth()));
        jQuery(this).css({
            "margin-left": -left,
            "margin-right": -right
        });
    });
}

// slider- scroll to content
$(function() {
    $('a[href^="#content"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            if ($(window).outerWidth() >= 1320) {
                event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            } else {
                event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top - 80
                }, 1000);
            }
        }
    });
});

// bootstrap carousel - init
$(document).ready(function() {
    $('.carousel').carousel({
        interval: 6000
    });
	if ($('.carousel-inner .item').is(':only-child')) {
		$('.carousel-control').hide();
	}
});

// slick - init
$('.slick-content-photo').slick({
	infinite: false,
	arrows: true,
	dots: true,
	autoplay: false,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1
});
$('.slick-footer-small').slick({
	infinite: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 4000,
	speed: 500,
	slidesToShow: 3,
	slidesToScroll: 1,
	responsive: [
		{
			breakpoint: 1630,
			settings: {
				slidesToShow: 2,
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
			}
		}
	]
});
$('.slick-footer-big').slick({
	infinite: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 4000,
	speed: 500,
	slidesToShow: 7,
	slidesToScroll: 1,
	responsive: [
		{
			breakpoint: 1630,
			settings: {
				slidesToShow: 5,
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
			}
		}
	]
});
