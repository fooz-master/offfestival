<header>
	<div class="container">
		<a href="/" class="logo">
			<img src="img/logo.svg" alt="Off Festival">
		</a>
		<div class="ticket">
			<span class="buy-date"><span>03 - 08.08.2018</span></span>
			<a href="#" class="buy-ticket">
				<?php include'_svg-ticket.php'; ?>
				<span>Kup bilet</span>
			</a>
		</div>
	</div>
</header>

<div class="header-sticky">
	<div class="container">
		<a href="/" class="logo-sticky">
			<img src="img/logo-small.svg" alt="Off Festival">
		</a>
		<div class="navbar mobile-button">
			<button type="button" class="navbar-toggle navbar-open" data-toggle="collapse" data-target="#mainmenu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	</div>
</div>

<div class="ticket-sticky">
	<a href="#" class="buy-ticket">
		<?php include'_svg-ticket.php'; ?>
	</a>
</div>
