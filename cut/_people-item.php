<article class="element-loop col-xs-12 col-sm-6">
	<div class="people-item">
		<a href="#" class="people-thumb">
			<img src="img/people-thumb.jpg" alt="">
		</a>
		<h2 class="people-link">
			<a href="#">
				Aldous Harding
			</a>
		</h2>
	</div>
</article>
