<!DOCTYPE html>
<html lang="pl">
<?php include'_head.php'; ?>

<body class="subpage">
	<?php include'_header.php'; ?>

	<div class="layout-grid">
		<div class="container">
			<?php include'_menu-main.php'; ?>

			<div class="slider-spacer"></div>

			<div class="column-layout-content">
				<main id="content">
					<section class="section-page-text">
						<div class="label">
							Lineup
						</div>
						<div class="content-text">
							<!-- <div class="lead">
								<h1>
									Pytania? Jesteśmy tutaj dla Ciebie.
								</h1>
								<p>
									W pierwszej kolejności zapoznaj się z naszą sekcją FAQ. Jeśli nadal masz wątpliwości, skontaktuj się z nami za pośrednictwem maila.
								</p>
							</div> -->
							<div class="panel-group text-uppercase">
								<div class="panel panel-default">
									<div class="panel-heading">
										<a href="#" class="collapsed">
											OFF Festival 2018
										</a>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<a href="#" class="collapsed">
											Dzień pierwszy
										</a>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<a href="#" class="collapsed">
											Dzień drugi
										</a>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<a href="#" class="collapsed">
											Dzień trzeci
										</a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<?php include'_footer.php'; ?>
			</div>
		</div>
	</div>
</body>

<?php include '_footer-scripts.php'; ?>

</html>
